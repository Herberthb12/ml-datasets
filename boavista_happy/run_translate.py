#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from mtranslate import translate


def main(text):    
    text = unicode(text).encode('utf8')
    to_translate = text
    print(to_translate)
    print(translate(to_translate))    

if __name__ == '__main__':
    main(sys.argv[1])
