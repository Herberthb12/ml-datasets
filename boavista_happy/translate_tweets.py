#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
import time 
import os.path
import os
import sys
import shutil
import csv
from tempfile import NamedTemporaryFile
import logging
from os import walk


sys.path.append('mtranslate')

from mtranslate import translate

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

logging.info("Translating collected tweet from tweets folder ...")
count_tweets = 0


def run_translate(path_file, new_file):
	global 	count_tweets
	
	# Read csv
	csv_path = path_file
	new_csv_path = new_file
	tempfile = NamedTemporaryFile(mode='w', delete=False)

	fields = ['date','username','to','replies','retweets','favorites','text','geo','mentions','hashtags','id','permalink']
	with open(csv_path, 'r') as csvfile, tempfile:
		reader = csv.DictReader(csvfile, fieldnames=fields)
		writer = csv.DictWriter(tempfile, fieldnames=fields)
		for row in reader:
			logging.info(">> Tweet - "+str(count_tweets))
			if not row['text'] == "text":
				#print(row['text'])
				#translate("\""+row['text']+"\"")
				text_eng = translate(row['text'])    
				row = {
					'date': row['date'], 
					'username': row['username'], 
					'to': row['to'], 
					'replies': row['replies'],
					'retweets': row['retweets'],
					'favorites': row['favorites'],
					'text': text_eng,
					'geo': row['geo'],
					'mentions': row['mentions'],
					'hashtags': row['hashtags'],
					'id': row['id'],
					'permalink': row['permalink'],
				}
				#print(row)
				writer.writerow(row)
			else:
				#header
				row = {
					'date': row['date'], 
					'username': row['username'], 
					'to': row['to'], 
					'replies': row['replies'],
					'retweets': row['retweets'],
					'favorites': row['favorites'],
					'text': row['text'],
					'geo': row['geo'],
					'mentions': row['mentions'],
					'hashtags': row['hashtags'],
					'id': row['id'],
					'permalink': row['permalink'],
				}
				writer.writerow(row)
		
			count_tweets = count_tweets + 1

	shutil.move(tempfile.name, new_csv_path)



if not os.path.isdir("mtranslate"):
	os.system("git clone https://github.com/mouuff/mtranslate.git")


first_level = True
for (dirpath, dirnames, filenames) in walk("tweets"):
	# creating dir for the results
	if first_level:
		#print(dirnames)	
		first_level = False
		list_d_new_names = []
		for d in dirnames:
			list_d_new_names.append(d+"_eng")

		#print(list_d_new_names)
		diff_lists_d = list(set(list_d_new_names) - set(dirnames))
		if len(diff_lists_d) > 0:
			for d in diff_lists_d:
				os.mkdir(dirpath+"/"+d)

	# Translating the tweets	
	if not dirpath == "tweets":
		logging.info(">> DIR PATH - "+dirpath)		
		for f in filenames:
			file_path = dirpath+"/"+f
			new_file_path = os.path.splitext(dirpath+"_eng"+"/"+f)[0]+"_eng.csv"
			#print(new_file_path)
			run_translate(file_path,new_file_path)
