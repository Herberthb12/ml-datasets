#!/usr/bin/env python3

import shlex, subprocess
from subprocess import Popen

import logging
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

import datetime
import time 

import os

logging.info("Running collect tweet ...")

# in string to search
bairros_bv = [	
	'\"Centro AND Roraima\"',
	'\"31 AND de AND Março AND Roraima\"',
	'\"Aeroporto AND Roraima\"',
	'\"Aparecida AND Roraima\"',
	'\"Bairro AND dos AND Estados AND Roraima\"',
	'\"São AND Francisco AND Roraima\"',
	'\"13 AND de AND Setembro AND Roraima\"',
	'\"Calungá AND Roraima\"',
	'\"Governador AND Aquilino AND Mota AND Roraima\"',
	'\"São AND Vicente AND Roraima\"',
	'\"Marechal AND Rondon AND Roraima\"',
	'\"Caçari AND Roraima\"',
	'\"Canarinho AND Roraima\"',
	'\"Paraviana AND Roraima\"',
	'\"São AND Pedro AND Roraima\"',
	'\"Asa AND Branca AND Roraima\"',
	'\"Alvorada AND Roraima\"',
	'\"Professora AND Araceli AND Souto AND Maior AND Roraima\"',
	'\"Araceli AND Roraima\"',
	'\"Bela AND Vista AND Roraima\"',
	'\"Buritis AND Roraima\"',
	'\"Caimbé AND Roraima\"',
	'\"Cambará AND Roraima\"',
	'\"Caranã AND Roraima\"',
	'\"Centenário AND Roraima\"',
	'\"Cinturão AND Verde AND Roraima\"',
	'\"Jardim AND Equatorial AND Roraima\"',
	'\"Senador AND Hélio AND Campos AND Roraima\"',
	'\"Hélio AND Campos AND Roraima\"',
	'\"Jardim AND Caranã AND Roraima\"',
	'\"Jardim AND Primavera AND Roraima\"',
	'\"Jardim AND Floresta AND Roraima\"',
	'\"Jardim AND Tropical AND Roraima\"',
	'\"Jóquei AND Clube AND Roraima\"',
	'\"Liberdade AND Roraima\"',
	'\"Murilo AND Teixeira AND Roraima\"',
	'\"Mecejana AND Roraima\"',
	'\"Nova AND Canaã AND Roraima\"',
	'\"Nova AND Cidade AND Roraima\"',
	'\"Operário AND Roraima\"',
	'\"Pintolândia AND Roraima\"',
	'\"Piscicultura AND Roraima\"',
	'\"Pricumã AND Roraima\"',
	'\"Raiar AND do AND Sol AND Roraima\"',
	'\"Doutor AND Aírton AND Rocha AND Roraima\"',
	'\"Aírton AND Rocha AND Roraima\"',
	'\"Doutor AND Silvio AND Botelho AND Roraima\"',
	'\"Silvio AND Botelho AND Roraima\"',
	'\"Doutor AND Silvio AND Leite AND Roraima\"',
	'\"Silvio AND Leite AND Roraima\"',
	'\"Santa AND Luzia AND Roraima\"',
	'\"Santa AND Tereza AND Roraima\"',
	'\"Tancredo AND Neves AND Roraima\"',
	'\"União AND Roraima\"',
	'\"Olímpico AND Roraima\"',
	'\"Laura AND Moreira AND Roraima\"',
	'\"São AND Bento AND Roraima\"',
	'\"Cidade AND Satélite AND Roraima\"',
]



# Date to start from
#date_lower = datetime.datetime(2020, 1, 1)
#date_upper = datetime.datetime(2020, 1, 31)
date_lower = datetime.datetime(2020, 4, 1)
date_upper = datetime.datetime(2020, 4, 30)
date_start = ""
date_until = ""
date_next  = ""
start_string = ""
next_day_string = ""
until_string = ""

count = 0
num_tweets = 0

def init_dates():
	
	global date_start, date_until, date_next, start_string, next_day_string, until_string
	
	date_start = date_lower
	date_until = date_upper
	date_next = date_start + datetime.timedelta(days=1)
	start_string = date_start.strftime("%Y-%m-%d")
	next_day_string = date_next.strftime("%Y-%m-%d")
	until_string = date_until.strftime("%Y-%m-%d")

# pip install GetOldTweets3
last_filename = ""

def collect_tweet(_name, _start_string, _next_day_string):
	global count
	global num_tweets
	global last_filename
	
	str_cmd = "GetOldTweets3 --querysearch "+_name+" --since "+_start_string+" --until "+_next_day_string
	logging.info("Running: "+str_cmd)
	
	args = shlex.split(str_cmd)	
	p = Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
	output, errors = p.communicate()
		
	if errors:
		logging.info(">> Going to sleep by 2 min found an ERROR")
		print(errors)
		time.sleep(60*2)
	else:		
		# Save on correct filename + append results // 
		logging.info(">> Saving results ...")
		
		str_cmd_sv = "sed -i \'1d\' output_got.csv" # POSIX
		args_sv = shlex.split(str_cmd_sv)		
		subprocess.call(args_sv)			
		
		# Append results
		f_tmp = open("output_got.csv", "r")
		f_results = open("output_csv_"+str(count)+".csv", "a")		
		f_results.write(f_tmp.read())		
		f_results.close()
		f_tmp.close()
		os.remove("output_got.csv")			
		
		# Info
		f_results = open("output_csv_"+str(count)+".csv", "r")		
		num_tweets = len(f_results.readlines())
		f_results.close()
		logging.info(">> Number of tweets collected are: "+str(num_tweets))		
		last_filename = "output_csv_"+str(count)+".csv"
		
		# exit(1)	
	

for name in bairros_bv:
	
	init_dates()

	while(date_next <= date_until):
		
		collect_tweet(name, start_string, next_day_string)
		
		#logging.info(">> Wait for the next day, going to sleep by 1min ")
		#time.sleep(60*1)
		# Add 1 to day after each step
		date_start += datetime.timedelta(days=1)
		date_next = date_start + datetime.timedelta(days=1)
		# Convert dates to string
		start_string = date_start.strftime("%Y-%m-%d")
		next_day_string = date_next.strftime("%Y-%m-%d")

	# add header to .csv file
	str_cmd = "sed -i \'1s\' ^  " + last_filename # POSIX
	with open(last_filename, 'r+') as f:
		content = f.read()
		f.seek(0, 0)
		f.write("date,username,to,replies,retweets,favorites,text,geo,mentions,hashtags,id,permalink\n" + content)	
	
		
		
	# count by places name
	count = count + 1
	num_tweets = 0	
		
	
